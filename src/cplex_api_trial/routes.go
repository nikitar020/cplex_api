package main

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		"ScriptExec",
		"GET",
		"/script/{workspace}/{scriptName}",
		ScriptExec,
	},
	Route{
		"ScriptExecAsync",
		"GET",
		"/script/{workspace}/{scriptName}/async",
		ScriptExecAsync,
	},
	Route{
		"ScriptExec",
		"GET",
		"/script/{workspace}/{scriptName}/{parameters}",
		ScriptExec,
	},
	Route{
		"ScriptExecAsync",
		"GET",
		"/script/{workspace}/{scriptName}/{parameters}/async",
		ScriptExecAsync,
	},
	Route{
		"ScriptStatus",
		"GET",
		"/status/{task}",
		ScriptStatus,
	},
	Route{
		"ScriptOutput",
		"GET",
		"/output/{task}",
		ScriptOutput,
	},

	// Create a new workspace, which corresponds to a directory
	Route{
		"CreateNewWorkspace",
		"GET",
		"/workspace/new",
		CreateNewWorkspace,
	},

	// Delete new workspace, which corresponds to a directory
	Route{
		"DeleteWorkspace",
		"GET",
		"/workspace/delete/{workspace}",
		DeleteWorkspace,
	},

	// Remove all work space directories (and their contents) if their creation date is older than X hours
	Route{
		"CleanupWorkspaces",
		"GET",
		"/workspace/cleanup",
		CleanupWorkspaces,
	},

	// Upload a file into a workspace dir
	Route{
		"Upload",
		"POST",
		"/upload/{workspace}",
		UploadToWorkspace,
	},

	// Download a file from a workspace dir
	Route{
		"Download",
		"GET",
		"/download/{workspace}/{filename}",
		DownloadFromWorkspace,
	},
}
