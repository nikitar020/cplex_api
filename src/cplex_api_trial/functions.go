package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"syscall"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/go-redis/redis"
	"github.com/gorilla/mux"
)

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func randSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

// RedisConnect Returns a new Redis connection client
func RedisConnect() *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     config.Redis,
		Password: "", // no password set
		DB:       0,  // use default DB
	})
}

// FetchScriptFile comment
func FetchScriptFile(taskFile string, w http.ResponseWriter, r *http.Request) {
	// TODO optimize code here

	vars := mux.Vars(r)
	task := vars["task"]
	pid := runningPids[task]
	taskStatusFile := config.TempFolder + string(filepath.Separator) + strconv.Itoa(pid) + STATUS_FILE
	taskOutputFile := config.TempFolder + string(filepath.Separator) + strconv.Itoa(pid) + OUTPUT_FILE
	log.Debug(fmt.Sprintf("Fetching data for task ID:%s PID:%d file: %s", task, pid, taskStatusFile))

	if _, err := os.Stat(taskStatusFile); os.IsNotExist(err) {
		log.Error(fmt.Sprintf("File not found [%s]", taskStatusFile))
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(200)
		if err = json.NewEncoder(w).Encode(CommandReply{
			Name:   vars["scriptName"],
			Output: "",
			Task:   task,
		}); err != nil {

			panic(err)
		}
		return
	}

	status, err := ioutil.ReadFile(taskStatusFile)
	if err != nil {
		log.Error(fmt.Sprintf("Cannot open status file [%s]", taskStatusFile))
		w.WriteHeader(200)
		if err = json.NewEncoder(w).Encode(CommandReply{
			Name:   vars["scriptName"],
			Output: "",
			Task:   task,
		}); err != nil {
			panic(err)
		}
	}

	output, err := ioutil.ReadFile(taskOutputFile)
	if err != nil {
		log.Error(fmt.Sprintf("Cannot open output file [%s]", taskOutputFile))
		w.WriteHeader(200)
		if err = json.NewEncoder(w).Encode(CommandReply{
			Name:   vars["scriptName"],
			Output: "",
			Task:   task,
		}); err != nil {
			panic(err)
		}
	}

	// check if PID is running
	processStatus := PROCESS_TERMINATED
	process, err := os.FindProcess(pid)
	if err == nil {
		err := process.Signal(syscall.Signal(0))
		if err == nil {
			processStatus = PROCESS_RUNNING
		}
	}

	// check if ERROR is present on output
	errorRegExp := regexp.MustCompile(config.ErrorRegex)
	if errorRegExp.FindStringIndex(string(output)) != nil {
		processStatus = PROCESS_ERROR
	}

	if taskFile == STATUS_FILE {
		err = json.NewEncoder(w).Encode(CommandReply{Name: vars["scriptName"], Output: string(status), Process: processStatus, Task: task})
	} else {
		err = json.NewEncoder(w).Encode(CommandReply{Name: vars["scriptName"], Output: string(output), Process: processStatus, Task: task})
	}
	if err != nil {
		log.Error(fmt.Sprintf("Error while encoding file output [%s]. %s", taskStatusFile, err.Error()))
		panic(err)
	}
}

// Returns list of paths with expiration time
func getListForCleanup() (files []string, errOut error) {
	baseWs := filepath.Base(config.WorkspaceFolder)
	errOut = filepath.Walk(config.WorkspaceFolder, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		pathWs := filepath.Base(path)
		if baseWs == pathWs {
			return nil
		}
		duration := time.Since(info.ModTime())
		if duration.Hours() > config.Xhours {
			files = append(files, path)
		}
		return nil
	})
	return
}
