package main

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/coreos/go-systemd/daemon"
	"gopkg.in/yaml.v2"
)

var config Config
var runningPids map[string]int

func init() {
	// log.SetFormatter(&log.JSONFormatter{})
	log.SetFormatter(&log.TextFormatter{FullTimestamp: true})

	// Output to stderr instead of stdout, could also be a file.
	log.SetOutput(os.Stderr)

	// Only log the warning severity or above.
	log.SetLevel(log.DebugLevel)

	runningPids = make(map[string]int)
}

func main() {

	router := NewRouter()
	rand.Seed(time.Now().UTC().UnixNano())

	// parse log file
	var filename string
	filename = os.Args[1]

	log.Info(fmt.Sprintf("Parsing config file %s", filename))

	source, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}

	if err := yaml.Unmarshal(source, &config); err != nil {
		panic(err)
	}

	// create tempfolder
	if _, err := os.Stat(config.TempFolder); os.IsNotExist(err) {
		if err := os.Mkdir(config.TempFolder, 0755); err != nil {
			panic(err)
		}
	}

	// create workspace folder
	if _, err := os.Stat(config.WorkspaceFolder); os.IsNotExist(err) {
		if err := os.Mkdir(config.WorkspaceFolder, 0755); err != nil {
			panic(err)
		}
	}

	if config.UseTls {
		caCert, err := ioutil.ReadFile(config.CaCertificate)
		if err != nil {
			log.Fatal(err)
		}

		caCertPool := x509.NewCertPool()
		caCertPool.AppendCertsFromPEM(caCert)

		tlsConfig := &tls.Config{
			ClientCAs: caCertPool,
			// NoClientCert
			// RequestClientCert
			// RequireAnyClientCert
			// VerifyClientCertIfGiven
			// RequireAndVerifyClientCert
			ClientAuth: tls.RequireAndVerifyClientCert,
		}

		tlsConfig.BuildNameToCertificate()

		server := &http.Server{
			Addr:      ":" + strconv.Itoa(config.Port),
			TLSConfig: tlsConfig,
			Handler:   router,
		}
		daemon.SdNotify(false, "READY=1")
		log.Info("Listening to https://0.0.0.0:" + strconv.Itoa(config.Port))
		log.Fatal(server.ListenAndServeTLS(config.ServerCertificate, config.ServerKey))
	} else {
		server := &http.Server{
			Addr:    ":" + strconv.Itoa(config.Port),
			Handler: router,
		}
		daemon.SdNotify(false, "READY=1")
		log.Info("Listening to http://0.0.0.0:" + strconv.Itoa(config.Port))
		log.Fatal(server.ListenAndServe())
	}

}
