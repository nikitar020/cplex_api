package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	log "github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
)

// UploadToWorkspace a file into the provided task
// Returns an error in case either the file or the task are not present/valid
func UploadToWorkspace(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	wsID := vars["workspace"]
	wsFolder := filepath.FromSlash(config.WorkspaceFolder + "/" + wsID)

	if _, err := os.Stat(wsFolder); os.IsNotExist(err) {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(400)
		if err := json.NewEncoder(w).Encode(HTTPReply{Code: 400, Error: "Error, cannot find workspace."}); err != nil {
			panic(err)
		}
		return
	}

	remoteFile, header, err := r.FormFile("file")
	if err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(400)
		if err := json.NewEncoder(w).Encode(HTTPReply{Code: 400, Error: "Error, cannot find file on request."}); err != nil {
			panic(err)
		}
		return
	}

	// close remoteFile on exit and check for its returned error
	defer func() {
		if err := remoteFile.Close(); err != nil {
			log.Error(fmt.Sprintf("Error while closing remote file"))
			panic(err)
		}
	}()

	// open output file
	// TODO check Filename with a RegExp
	localFile, err := os.Create(filepath.FromSlash(wsFolder + "/" + header.Filename))
	if err != nil {
		panic(err)
	}
	// close localFile on exit and check for its returned error
	defer func() {
		if err := localFile.Close(); err != nil {
			log.Error(fmt.Sprintf("Error while closing local file"))
			panic(err)
		}
	}()

	log.Debug(fmt.Sprintf("Uploaded %s into WS %s", header.Filename, wsFolder))
	io.Copy(localFile, remoteFile)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(200)
	return
}

// DownloadFromWorkspace a file into the provided task
// Returns an error in case either the file or the task are not present/valid
func DownloadFromWorkspace(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	wsID := vars["workspace"]
	localFileName := vars["filename"]
	localFilePath := filepath.FromSlash(config.WorkspaceFolder + "/" + wsID + "/" + localFileName)

	// check for file existence
	if _, err := os.Stat(localFilePath); os.IsNotExist(err) {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(404)
		if err := json.NewEncoder(w).Encode(HTTPReply{Code: 404, Error: "Error, cannot find requested file"}); err != nil {
			panic(err)
		}
		log.Error(fmt.Sprintf("Error, cannot find requested file [%s].", localFilePath))
		return
	}

	// open the file
	localFile, err := os.Open(localFilePath)
	if err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(400)
		if err := json.NewEncoder(w).Encode(HTTPReply{Code: 400, Error: "Error, cannot open requested file"}); err != nil {
			panic(err)
		}
		log.Error(fmt.Sprintf("Error, cannot open requested file [%s].", localFilePath))
		return
	}
	// close localFile on exit and check for its returned error
	defer func() {
		if err := localFile.Close(); err != nil {
			log.Error(fmt.Sprintf("Error while closing local file"))
			panic(err)
		}
	}()

	//Get the Content-Type of the file
	//Create a buffer to store the header of the file in
	fileHeader := make([]byte, 512)
	//Copy the headers into the FileHeader buffer
	localFile.Read(fileHeader)
	//Get content type of file
	fileContentType := http.DetectContentType(fileHeader)

	//Get the file size
	fileStat, _ := localFile.Stat()                    //Get info from file
	fileSize := strconv.FormatInt(fileStat.Size(), 10) //Get file size as a string

	//Send the headers
	w.Header().Set("Content-Disposition", "attachment; filename="+localFileName)
	w.Header().Set("Content-Type", fileContentType)
	w.Header().Set("Content-Length", fileSize)

	//Send the file
	localFile.Seek(0, 0)
	io.Copy(w, localFile)

	log.Debug(fmt.Sprintf("Downloaded %s", localFilePath))
	return
}
