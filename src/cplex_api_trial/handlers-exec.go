package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	log "github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
)

const (
	STATUS_FILE        = ".status"
	OUTPUT_FILE        = ".output"
	PROCESS_RUNNING    = "running"
	PROCESS_TERMINATED = "terminated"
	PROCESS_ERROR      = "error"
)

func ScriptExec(w http.ResponseWriter, r *http.Request) {
	Script(false, w, r)
}

func ScriptExecAsync(w http.ResponseWriter, r *http.Request) {
	Script(true, w, r)
}

func Script(async bool, w http.ResponseWriter, r *http.Request) {
	var err error
	vars := mux.Vars(r)
	scriptFile := config.LookupFolder + string(filepath.Separator) + vars["scriptName"]
	log.Debug(fmt.Sprintf("Trying to call file %s", scriptFile))
	scriptParameters := vars["parameters"]
	if scriptParameters != "" {
		log.Debug(fmt.Sprintf("With parameters %s", scriptParameters))
	}

	if _, err := os.Stat(scriptFile); os.IsNotExist(err) {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422)
		if err = json.NewEncoder(w).Encode(err); err != nil {
			log.Error(fmt.Sprintf("Script file not found [%s]. %s", scriptFile, err.Error))
			panic(err)
		}
		return
	}

	var output []byte
	var cmd *exec.Cmd

	// instantiate command
	if scriptParameters != "" {
		cmd = exec.Command(scriptFile, strings.Split(scriptParameters, " ")...)
	} else {
		cmd = exec.Command(scriptFile)
	}

	// push environment variables
	wsID := vars["workspace"]
	env := os.Environ()
	env = append(env, fmt.Sprintf("WORKSPACE=%s", wsID))
	env = append(env, fmt.Sprintf("TEMP_FOLDER=%s", config.TempFolder))
	env = append(env, fmt.Sprintf("WORKSPACE_FOLDER=%s", config.WorkspaceFolder))
	env = append(env, fmt.Sprintf("LOOKUP_FOLDER=%s", config.LookupFolder))
	cmd.Env = env

	// run command
	if async == true {
		err = cmd.Start()
		go func() {
			cmd.Wait()
		}()
	} else {
		output, err = cmd.CombinedOutput()
	}
	if err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422)
		if err := json.NewEncoder(w).Encode(err); err != nil {
			log.Error(fmt.Sprintf("Error while executing file [%s]. %s", scriptFile, err.Error))
			panic(err)
		}
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	taskId := randSeq(16)
	runningPids[taskId] = cmd.Process.Pid
	if async == true {
		err = json.NewEncoder(w).Encode(CommandReply{Name: vars["scriptName"], Process: PROCESS_RUNNING, Task: taskId})
	} else {
		err = json.NewEncoder(w).Encode(CommandReply{Name: vars["scriptName"], Output: string(output), Process: PROCESS_TERMINATED, Task: taskId})
	}

	if err != nil {
		log.Error(fmt.Sprintf("Error while encoding script output [%s]. %s", scriptFile, err.Error))
		panic(err)
	}
}

func ScriptStatus(w http.ResponseWriter, r *http.Request) {
	FetchScriptFile(STATUS_FILE, w, r)
}

func ScriptOutput(w http.ResponseWriter, r *http.Request) {
	FetchScriptFile(OUTPUT_FILE, w, r)
}
