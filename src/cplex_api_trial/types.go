package main

// HTTPReply Used to instantiate a reply messages at HTTP level
type HTTPReply struct {
	Code    int    `json:"code"`
	Error   string `json:"error"`
	Message string `json:"message"`
}

// CommandReply Used to instantiate a command reply
type CommandReply struct {
	Name      string `json:"name"`
	Output    string `json:"output"`
	Task      string `json:"task"`
	Workspace string `json:"workspace"`
	Process   string `json:"process"`
	Error     string `json:"error"`
}

// Config Configuration structure
type Config struct {
	UseTls            bool
	CaCertificate     string
	ServerCertificate string
	ServerKey         string
	Port              int
	LookupFolder      string
	TempFolder        string
	WorkspaceFolder   string
	ErrorRegex        string
	Redis             string
	Xhours            float64
}
