package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"path/filepath"

	log "github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
)

// CreateNewWorkspace creates a new directory which will be connected to a task
func CreateNewWorkspace(w http.ResponseWriter, r *http.Request) {
	wsID := randSeq(16)
	log.Info(fmt.Sprintf("Creating a new workspace of ID %s", wsID))

	wsFolder := filepath.FromSlash(config.WorkspaceFolder + "/" + wsID)
	if err := os.Mkdir(wsFolder, 0755); err != nil {
		panic(err)
	}
	log.Debug(fmt.Sprintf("Created a new directory at %s", wsFolder))

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(200)

	// we only return the TASK ID
	if err := json.NewEncoder(w).Encode(CommandReply{Workspace: wsID}); err != nil {
		panic(err)
	}
	return
}

// DeleteWorkspace creates a new directory which will be connected to a task
func DeleteWorkspace(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	wsID := vars["workspace"]
	log.Info(fmt.Sprintf("Delete workspace of ID %s", wsID))

	wsFolder := filepath.FromSlash(config.WorkspaceFolder + "/" + wsID)
	if err := os.RemoveAll(wsFolder); err != nil {
		panic(err)
	}
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(200)

	// we only return the removed TASK ID
	if err := json.NewEncoder(w).Encode(CommandReply{Workspace: wsID}); err != nil {
		panic(err)
	}
	return
}

// CleanupWorkspaces removes folders with expiration time
func CleanupWorkspaces(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	files, err := getListForCleanup()

	if err != nil {
		log.Error(err.Error())
	}

	for _, file := range files {
		if err := os.RemoveAll(file); err != nil {
			panic(err)
		}
		log.Debug(fmt.Sprintf("Deleted a file/directory at %s", file))
	}

	w.WriteHeader(200)
	if err := json.NewEncoder(w).Encode(CommandReply{Process: "OK"}); err != nil {
		panic(err)
	}
	return
}
