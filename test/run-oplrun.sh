#!/bin/bash

PID=$$
OPLRUN_PATH=/opt/ibm/ILOG/CPLEX_Studio1271/opl/bin/x86-64_linux
LD_LIBRARY_PATH=$OPLRUN_PATH
export LD_LIBRARY_PATH

echo "runnning" > $TEMP_FOLDER/$PID.status
$OPLRUN_PATH/oplrun $WORKSPACE_FOLDER/$WORKSPACE/$1 $WORKSPACE_FOLDER/$WORKSPACE/$2 > $TEMP_FOLDER/$PID.output
cat $TEMP_FOLDER/$PID.output
echo "terminated" > $TEMP_FOLDER/$PID.status
