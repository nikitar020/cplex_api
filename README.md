# CPLEX API Server

## How to build and start after clonning
cd cplex_api_trial
### Install packages
./r i
### Build
./r b
### Run the server
./r
### Cleanup end-point
/workspace/cleanup

## Provided API functions

### Create a new workspace
Create a new workspace

```bash
curl -vv  http://cplex1-evo:8080/workspace/new
```

Returns a JSON string with the created workspace:
```json
{"name":"","output":"","task":"","workspace":"sxdylGAITRnZnYsc","process":"","error":""}
```

### Delete a workspace
Delete a workspace.
If the workspace does not exists, the server doe not return any error.
This operation will delete ALL workspace content.
There is no check about any running processes which might use the workspace.
Always delete the workspace after having retrieved all the information.
Workspace leaved on the server **are not** removed, so they can fill the entire server filesystem space.
```bash
curl -vv http://cplex1-evo:8080/workspace/delete/OCyFwMnUlhdHFOfa
```

Returns a JSON string with the created workspace:
```json
{"name":"","output":"","task":"","workspace":"sxdylGAITRnZnYsc","process":"","error":""}
```

### Upload
Upload a file into a workspace.
The HTTP request mus be POST and multiform field must be named **file**.
If the file is in use, will throw an error.
If the file exists, will be overwritten.
```bash
curl -vv -F file=@./model_max_matching_loc.mod http://cplex1-evo:8080/upload/sxdylGAITRnZnYsc
```

### Download
Download a file from a workspace.
The file to download must be knonw in advance.
If the specified file is not present, throws an error.
```bash
curl -vv http://cplex1-evo:8080/download/WkEhjcJZPfTWLkie/output.csv
```

The answer contains the requested file.

### Run syncronously the opl-run.sh script with parameters
Run the opl-run command with two input parameters.  Output will be produced on the workspace directory.
Parameters must be divided by **%20** (HTML URL encode value for a blank space).
If the script is not present, will throw an error.
There is no concurrency check, due to this is advisable to run two processes on two different workspaces.
```bash
curl -vv  "http://cplex1-evo:8080/script/sxdylGAITRnZnYsc/run-oplrun.sh/model_max_matching_loc.mod%20data_max_matching.dat"
```

The result is a JSON string with the output produced by the script.
```json
{"name":"run-oplrun.sh","output":"\n\u003c\u003c\u003c setup\n\n\n\u003c\u003c\u003c generate\n\nCPXPARAM_TimeLimit                               60\nCPXPARAM_MIP_Strategy_CallbackReducedLP          0\nFound incumbent of value 0.000000 after 0.00 sec. (0.00 ticks)\nTried aggregator 1 time.\nMIP Presolve eliminated 15 rows and 6 columns.\nAll rows and columns eliminated.\nPresolve time = 0.00 sec. (0.01 ticks)\n\nRoot node processing (before b\u0026c):\n  Real time             =    0.00 sec. (0.01 ticks)\nParallel b\u0026c, 10 threads:\n  Real time             =    0.00 sec. (0.00 ticks)\n  Sync time (average)   =    0.00 sec.\n  Wait time (average)   =    0.00 sec.\n                          ------------\nTotal (root+branch\u0026cut) =    0.00 sec. (0.01 ticks)\n\n\u003c\u003c\u003c solve\n\n\nOBJECTIVE: 56.19957\nBEST BOUND: 56.199573\n\n\u003c\u003c\u003c post process\n\n\n\u003c\u003c\u003c done\n\n","task":"ajHIMKwoKeJTkKqo","workspace":"","process":"terminated","error":""}
```

### Run asyncronously the opl-run.sh script with parameters
Same as sync run, but the process terminates immediately.
It's up to the user to check whether the script is terminated or not (calling an other API).
```bash
curl -vv  "http://cplex1-evo:8080/script/sxdylGAITRnZnYsc/run-oplrun.sh/model_max_matching_loc.mod%20data_max_matching.dat/async"
```

The return value is a JSON string with a TASK id. This ID is a unique value which should be used to identify a running (or already terminated) execution.
```json
{"name":"run-oplrun.sh","output":"","task":"gsvZPBCNzNCUIoRj","workspace":"","process":"running","error":""}
```

### Get the running status of a script, given the TASK ID
When the execution of the script is async, below call can be used to retrieve the status of a script.
For the run-oplrun.sh script, the value returned can be only:
* running
* terminated

There is no check about the "real" status of the execution (i.e. the call does not know if the process is terminated). The value is taken from the script itself.

```bash
curl -vv http://cplex1-evo:8080/status/gsvZPBCNzNCUIoRj
```

The server answer is a JSON string containing the process status on the **process** field:
```json
{"name":"","output":"terminated\n","task":"gsvZPBCNzNCUIoRj","workspace":"","process":"terminated","error":""}
```

### Get the running output of a script, given the TASK ID
When the execution of the script is async, below call can be used to retrieve the output of a script.

```bash
curl -vv http://cplex1-evo:8080/output/gsvZPBCNzNCUIoRj
```

The answer is a JSON string which contains the script output produced so far.
```json
{"name":"","output":"\n\u003c\u003c\u003c setup\n\n### OPL exception: File \"/srv/cplexapi/sxdylGAITRnZnYsc/data_max_matching.dat\" not found.\n","task":"gsvZPBCNzNCUIoRj","workspace":"","process":"terminated","error":""}
```

## Usage Example (CURL)

```bash
curl -vv  http://cplex1-evo:8080/workspace/new
curl -vv -F file=@./model_max_matching_loc.mod http://cplex1-evo:8080/upload/WkEhjcJZPfTWLkie
curl -vv -F file=@./data_max_matching.dat http://cplex1-evo:8080/upload/WkEhjcJZPfTWLkie
curl -vv  "http://cplex1-evo:8080/script/WkEhjcJZPfTWLkie/run-oplrun.sh/model_max_matching_loc.mod%20data_max_matching.dat"
curl -vv http://cplex1-evo:8080/download/WkEhjcJZPfTWLkie/output.csv
```